/* Add owner Campaign Email ID */
function addCampaignOwnerEmail(){
  let insertEmail = document.getElementById("CampaignOwnerEmail").value;

  const insertDiv = document.createElement('div');
  let countEmail = document.getElementById("ListCampaignOwnerEmail").childElementCount;
  let useIdentifier = "CampaignOwnerEmail"+countEmail
  insertDiv.setAttribute("id", useIdentifier);
  insertDiv.className = 'display-flex flex_align-center flex_justify-space-between';

  insertDiv.innerHTML = `
    <p>${insertEmail}</p>
    <img src='./public/assets/images/remove-cross.svg' class=${useIdentifier} alt='' onClick="removeCampaignOwnerEmail(this)"/>
  `;

  document.getElementById('ListCampaignOwnerEmail').appendChild(insertDiv);
}
function removeCampaignOwnerEmail(){
  var classIdentifier = event.target.className;
  var objRemove = document.getElementById(classIdentifier);
  objRemove.remove();
}

//Open Lead file
document.getElementById("fileUploadLead").addEventListener("change", function() {
  document.getElementById("countfileUploadLead").innerHTML=this.files.length +" file uploaded";
});

//Open recording file
document.getElementById("fileUploadRecord").addEventListener("change", function() {
  document.getElementById("countfileUploadRecord").innerHTML=this.files.length +" file uploaded";
});


/* Request Service Type */
document.getElementById("fullQC-yes").addEventListener("change", () => {
  document.getElementById("phone-data-verify-yes").checked=true;
  document.getElementById("recording-review-yes").checked=true;
  document.getElementById("data_correction-yes").checked=true;
  document.getElementById("recording-forensic-yes").checked=true;
  document.getElementById("match-yes").checked=true;
})

/* Custom QnA */
function addCustomQnA(){
  let insertEmail = document.getElementById("CustomQnA").value;

  const insertDiv = document.createElement('div');
  let countEmail = document.getElementById("ListCustomQnA").childElementCount;
  let useIdentifier = "ListCustomQnA"+countEmail
  insertDiv.setAttribute("id", useIdentifier);
  insertDiv.className = 'display-flex flex_align-center flex_justify-space-between';

  insertDiv.innerHTML = `
    <p>${insertEmail}</p>
    <img src='./public/assets/images/remove-cross.svg' class=${useIdentifier} alt='' onClick="removeCustomQnA(this)"/>
  `;

  document.getElementById('ListCustomQnA').appendChild(insertDiv);
}
function removeCustomQnA(){
  var classIdentifier = event.target.className;
  var objRemove = document.getElementById(classIdentifier);
  objRemove.remove();
}

/* Asset Specification */

function assetSpecification(type){
  console.log("---------------------------")
  console.log(type)
  document.getElementById("Uploadfile-asset").classList.add('display-none');
  document.getElementById("PasteUrl-asset").classList.add('display-none');
  if(type==='Uploadfile'){
    document.getElementById(type+"-asset").classList.remove('display-none')
  }else if(type==='PasteUrl'){
    document.getElementById(type+"-asset").classList.remove('display-none')
  }
}


/* Job Title */
function addJobTitile(){
  let insertEmail = document.getElementById("jobTitle").value;

  const insertDiv = document.createElement('div');
  let countEmail = document.getElementById("listJobTitle").childElementCount;
  let useIdentifier = "jobTitle"+countEmail
  insertDiv.setAttribute("id", useIdentifier);
  insertDiv.className = 'display-flex flex_align-center flex_justify-space-between';

  insertDiv.innerHTML = `
    <p>${insertEmail}</p>
    <img src='./public/assets/images/remove-cross.svg' class=${useIdentifier} alt='' onClick="removeJobTitile(this)"/>
  `;

  document.getElementById('listJobTitle').appendChild(insertDiv);
}
function removeJobTitile(){
  console.log("remove job title")
  var classIdentifier = event.target.className;
  var objRemove = document.getElementById(classIdentifier);
  objRemove.remove();
}
function jobTitleSwitchChange(){
  console.log("jobTitleSwitchChange")
  let checkedCheck = document.getElementById("jobTitleSwitch").checked;
  if(checkedCheck===true){
    document.getElementById("JobTitle-on-view").classList.remove("display-none")
    document.getElementById("JobTitle-off-view").classList.add("display-none")
  }else{
    document.getElementById("JobTitle-on-view").classList.add("display-none")
    document.getElementById("JobTitle-off-view").classList.remove("display-none")
  }
}

/* geography Change */
function addGeography(){
  let insertEmail = document.getElementById("GeographyTitle").value;

  const insertDiv = document.createElement('div');
  let countEmail = document.getElementById("listGeographyTitle").childElementCount;
  let useIdentifier = "GeographyTitle"+countEmail
  insertDiv.setAttribute("id", useIdentifier);
  insertDiv.className = 'display-flex flex_align-center flex_justify-space-between';

  insertDiv.innerHTML = `
    <p>${insertEmail}</p>
    <img src='./public/assets/images/remove-cross.svg' class=${useIdentifier} alt='' onClick="removeGeographyTitle(this)"/>
  `;

  document.getElementById('listGeographyTitle').appendChild(insertDiv);
}
function removeGeographyTitle(){
  var classIdentifier = event.target.className;
  var objRemove = document.getElementById(classIdentifier);
  objRemove.remove();
}
function geographySwitchChange(){
  let checkedCheck = document.getElementById("geographySwitch").checked;
  if(checkedCheck===true){
    document.getElementById("geography-on-view").classList.remove("display-none")
    document.getElementById("geography-off-view").classList.add("display-none")
  }else{
    document.getElementById("geography-on-view").classList.add("display-none")
    document.getElementById("geography-off-view").classList.remove("display-none")
  }
}

/* Sidebar click */
function activeClick(item){
   elements = document.getElementsByClassName("sidebar-elemt");
   for(i=0;i<elements.length;i++){
     elements[i].classList.remove("activeElement");
   }
   item.classList.add("activeElement");
}